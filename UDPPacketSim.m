close all force;
clear all;

localHost = '127.0.0.1';
localPort = 8888;
remoteHost = '127.0.0.1';
remotePort = 4444;

% try
%     echoudp("on",remotePort);
% catch
% end

udpObj = udpport('LocalHost', localHost, 'LocalPort', localPort);
configureCallback(udpObj,'byte',6,@udpSimEvent);

simData = [uint16([ones(1,25).*1024,ones(1,5).*800,linspace(801,2048,50),ones(1,20).*2100]);...                         %frontSen
        uint16([ones(1,25).*2048,ones(1,5).*3096,linspace(3096,1024,50),ones(1,20).*2100]);...                          %leftSensor
        uint16([ones(1,25).*2048,ones(1,5).*1024,linspace(1024,3096,50),ones(1,20).*3100]);...                          %rightSensor
        uint16([(2200 - 75.*rand(1,25)),(2200 - 37.*rand(1,25)),(2200 - 13.*rand(1,25)),(2200 - 0.*rand(1,25))]);...    %batteryVal
        uint16([linspace(0,100,25),linspace(105,183,25),linspace(180,105,25),linspace(105,50,25)]);...                  %rpmSensor
        uint16(randi(500,1,100));...    %xAccelVal
        uint16(randi(500,1,100));...    %yAccelVal
        uint16(randi(500,1,100));];     %zAccelVal

dataCont = zeros(1,size(simData,1)*2);
udpObj.UserData.simData = simData;
udpObj.UserData.dataCont = dataCont;
udpObj.UserData.remoteHost = remoteHost;
udpObj.UserData.remotePort = remotePort;

%while(1)
%    x = input();
    %pause(0.001);
%    disp([CH, DT])
%end

str = '';

while (isempty(str))
    prompt = 'Press any key to exit:  ';
    str = input(prompt,'s');
end

clear;

function udpSimEvent(src,~)

    %%%%%% First look for the start and end condition
    simData = src.UserData.simData;
    dataCont = src.UserData.dataCont;
    if (src.NumBytesAvailable)
        recV = read(src, src.NumBytesAvailable,'uint8');
        if(((recV(1) == 'S') && (recV(2) == ':')) && ((recV(end) == 'E') && (recV(end-1) == ':')))
            disp([num2str(recV(3)),' ', num2str(recV(4))]);

            for iSplitRun = 1:size(simData,1)
                dataCont(iSplitRun*2-1) = uint8(bitshift(simData(iSplitRun,1),-8));
                dataCont(iSplitRun*2)   = uint8(bitand(simData(iSplitRun,1),255));
                simData(iSplitRun,:) = circshift(simData(iSplitRun,:),1);
                src.UserData.simData = simData;
                src.UserData.dataCont = dataCont;
            end

            cmdToRemote = ['C',':',dataCont,':','E'];
            write(src,cmdToRemote,'uint8',src.UserData.remoteHost,src.UserData.remotePort)
        end
    end
end






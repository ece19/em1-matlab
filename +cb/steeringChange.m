function steeringChange(src, callbackdata)
    data = guidata(src);

    data.steering = uint8(callbackdata.Value + 100);
    
    guidata(src, data);
end
function udpEvent(src, ~)
  global envHandle
  
  if (src.NumBytesAvailable)
    data = guidata(envHandle.UIFigure);
      
    payload = read(src, 1, 'uint8');
    bytes = read(src, src.NumBytesAvailable, 'uint8');
    
    if(((bytes(1) == 'C') && (bytes(2) == ':')) && ((bytes(payload) == 'E') && (bytes(payload - 1) == ':')))
        
        frontSensor     = bitshift(uint16(bytes(4)), 8) + uint16(bytes(3));
        leftSensor      = bitshift(uint16(bytes(6)), 8) + uint16(bytes(5));
        rightSensor     = bitshift(uint16(bytes(8)), 8) + uint16(bytes(7));
        
        data.t = circshift(data.t, -1);
        data.t(end) = now;
    
        data.frontSensor = circshift(data.frontSensor, -1);
        data.frontSensor(end) = frontSensor;
    
        data.leftSensor = circshift(data.leftSensor, -1);
        data.leftSensor(end) = leftSensor;
    
        data.rightSensor = circshift(data.rightSensor, -1);
        data.rightSensor(end) = rightSensor;
        
        data.battery    = double(bitshift(uint16(bytes(10)), 8) + uint16(bytes(9))) / 4095;
        data.rpmSensor  = double(bitshift(uint16(bytes(12)), 8) + uint16(bytes(11))) * 1e-3;
        
        gyroX           = (bitshift(uint16(bytes(14)), 8) + uint16(bytes(13)) - 32768) * 180 / 32768;  
        gyroY           = (bitshift(uint16(bytes(16)), 8) + uint16(bytes(15)) - 32768) * 180 / 32768;
        gyroZ           = (bitshift(uint16(bytes(18)), 8) + uint16(bytes(17)) - 32768) * 180 / 32768;
        posX            = (bitshift(uint16(bytes(20)), 8) + uint16(bytes(19)) - 32768);  
        posY            = (bitshift(uint16(bytes(22)), 8) + uint16(bytes(21)) - 32768);
        
        data.gyro_x = circshift(data.gyro_x, -1);
        data.gyro_x(end) = gyroX;
        
        data.gyro_y = circshift(data.gyro_y, -1);
        data.gyro_y(end) = gyroY; 
        
        data.gyro_z = circshift(data.gyro_z, -1);
        data.gyro_z(end) = gyroZ;
        
        data.pos_x = circshift(data.pos_x, -1);
        data.pos_x(end) = posX; 
        
        data.pos_y = circshift(data.pos_y, -1);
        data.pos_y(end) = posY;

        data.refSpeed   = double(bytes(24)) / 256 * 2.5; 
        data.ccEnable   = bitand(uint8(bytes(23)), 0b00000001);
        data.ccSet      = (bitshift(bitand(uint8(bytes(23)), 0b00000010), -1));
        
        env.setRefSpeed(data.refSpeed);
        env.toggleLight(data.ccEnable, 1);
        env.toggleLight(data.ccSet, 3);

    end
    
    guidata(envHandle.UIFigure, data);
  end
end
function decrease(src, callbackdata)
%DECREASE 

  data = guidata(src);
  
  data.decrease = uint8(src.Value);

  guidata(src, data)

end

function speedChange(src, callbackdata)
    data = guidata(src);

    speed = callbackdata.Value;
    
    if (speed > 0)
       speed = speed + 150; 
    elseif (speed < 0)
       speed = speed + 100;
    else
       speed = speed + 125; 
    end
    
    data.speed = uint8(speed);
    
    guidata(src, data);
end
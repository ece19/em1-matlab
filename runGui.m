close all force
clear all
%%% Run your GUI-Creation
%%%Create GUI

global envHandle;

envHandle = env.createGUI(); %Create your GUI HERE in this function
data = guidata(envHandle.UIFigure); % get GUI data

% toolbox checker function

env.checkToolboxes();

%initialize UDP
localhost = "192.168.4.2";
local_port = 1505;
remotehost = "192.168.4.1";
remote_port = 1504;

udpObj = udpport('LocalHost', localhost, 'LocalPort', local_port);
configureCallback(udpObj, 'byte', 20, @cb.udpEvent);

%initialize Environment
data.runProc = 1;
data.RefRate = 50;

data.reload = 0;
data.increase = 0;
data.decrease = 0;
data.cc = 0;

data.ccEnable = 0; 
data.ccSet = 0; 

data.cancle_lamp = false; 
data.resume_lamp = false; 

data.start_time = now;
data.graph_length = 500;
data.t = zeros(1, data.graph_length)'; % seconds

%%:
data.steering = 100;
data.speed = 125;
data.Fc = 60;                                   % hertz

data.frontSensor    = ones(1, data.graph_length);
data.leftSensor     = ones(1, data.graph_length);
data.rightSensor    = ones(1, data.graph_length);
data.gyro_x         = ones(1, data.graph_length);
data.gyro_y         = ones(1, data.graph_length);
data.gyro_z         = ones(1, data.graph_length);
data.pos_x          = ones(1, data.graph_length);
data.pos_y          = ones(1, data.graph_length);
data.pos_z          = ones(1, data.graph_length);

data.battery = 0;
data.rpmSensor = 0;

%%%%
guidata(envHandle.UIFigure,data); % save GUI data

plot(data.t, data.frontSensor,  'Parent', envHandle.tab1.ir_front);
plot(data.t, data.leftSensor,   'Parent', envHandle.tab1.ir_left);
plot(data.t, data.rightSensor,  'Parent', envHandle.tab1.ir_right);
plot(data.t, data.gyro_x,       'Parent', envHandle.tab2.gyro_x_plot);
plot(data.t, data.gyro_y,       'Parent', envHandle.tab2.gyro_y_plot);
plot(data.t, data.gyro_z,       'Parent', envHandle.tab2.gyro_z_plot);
plot(data.t, data.pos_x,      'Parent', envHandle.tab2.pos_x_plot);
plot(data.t, data.pos_y,      'Parent', envHandle.tab2.pos_y_plot);

envHandle.ir_front.Children.PickableParts  = 'none';
envHandle.ir_left.Children.PickableParts  = 'none';
envHandle.ir_right.Children.PickableParts  = 'none';

drawnow;

while(data.runProc)
    data = guidata(envHandle.UIFigure);

    if data.t(1) > 0
      data.start_time = data.t(1);
    end

    time = data.t - data.start_time;
    time(time<0) = 0;

    data = env.setSensorGraph(data, time);
    data = env.setGyroGraph(data, time);
    data = env.setPosGraph(data, time);

    env.setBatteryGraph(data.battery);
    env.setSpeedGraph(data.rpmSensor);

    % Add inc and dec to data.cc
    data.cc = bitor(data.cc, bitshift(data.increase, 4));
    data.cc = bitor(data.cc, bitshift(data.decrease, 5));
    
    % setting speed and steering into udp package
    cmdToRemote = ['S',':', data.steering, data.speed, data.reload, data.cc, ':','E'];
    % writing upd package onto the car
    write(udpObj,cmdToRemote,'uint8', remotehost, remote_port);

    % reset Reload flag
    data.reload = 0;
    data.cc = bitand(data.cc, 0b00000000);
    
    guidata(envHandle.UIFigure,data);
    drawnow;

    pause(1/data.RefRate);
end

close all force
function data = setPosGraph (data, time)
    global envHandle
    
    envHandle.tab2.pos_x_plot.Children.XData = time;
    envHandle.tab2.pos_y_plot.Children.XData = time;
    envHandle.tab2.pos_z_plot.Children.XData = time;
    
    envHandle.tab2.pos_x_plot.Children.YData   = data.pos_x;
    envHandle.tab2.pos_y_plot.Children.YData   = data.pos_y;
    envHandle.tab2.pos_z_plot.Children.YData   = data.pos_z;
    
    return
end
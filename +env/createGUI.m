function envHandle = createGUI()

window_size = [1024, 642];
tab_size = [window_size(1), window_size(2) - 24];
title_space = 24;

envHandle.UIFigure = uifigure('Visible', 'on');
envHandle.UIFigure.Position = [50, 50, window_size];
envHandle.UIFigure.Name = "EM1 GUI";
envHandle.UIFigure.CloseRequestFcn = @cb.closereq;

envHandle.tabs = uitabgroup(envHandle.UIFigure);
envHandle.tabs.Position = [0, 0, window_size];

tab1.tab = uitab(envHandle.tabs, "Title", "Steuerung");
tab2.tab = uitab(envHandle.tabs, "Title", "Positionsdaten");
tab3.tab = uitab(envHandle.tabs, "Title", "Tempomat");

reload_button_size = [100, 22];

% tab1 - speed  ------------------------------------------------------------------------------------------------------------------

speed_panel_size = [2/3 * tab_size(2) - 60, 2/3 * tab_size(2) - reload_button_size(2)];
slider_size = [speed_panel_size(1) - 36, 60];
gauge_size = [2/3 * speed_panel_size(2) - title_space, 2/3 * speed_panel_size(2) - title_space];

tab1.speed_panel = uipanel(tab1.tab);
tab1.speed_panel.Position = [24, tab_size(2) - speed_panel_size(2) - 24, speed_panel_size(1), speed_panel_size(2)];
tab1.speed_panel.BorderType = 'none';
tab1.speed_panel.Title = 'Geschwindigkeit';
%tab1.speed_panel.FontSize = 18;

tab1.rpm = uigauge(tab1.speed_panel);
tab1.rpm.Position = [(speed_panel_size(1) - gauge_size(1))/2, speed_panel_size(2) - gauge_size(2) - title_space, gauge_size(1), gauge_size(2)];
tab1.rpm.Limits = [0, 5];

tab1.speed_control = uislider(tab1.speed_panel);
tab1.speed_control.Position = [18, 80, slider_size(1), 3];
tab1.speed_control.Limits = [-100, 100];
tab1.speed_control.ValueChangedFcn = @cb.speedChange;

% tab1 - steering -----------------------------------------------------------------------------------------------------------------

steering_panel_size = [2/3 * tab_size(2) - 60, 1/3 * tab_size(2) - 3 * 24];
slider_size = [steering_panel_size(1) - 36, 60];

tab1.steering_panel = uipanel(tab1.tab);
tab1.steering_panel.Position = [24, 24 + reload_button_size(2), steering_panel_size(1), steering_panel_size(2)];
tab1.steering_panel.BorderType = 'none';
tab1.steering_panel.Title = 'Lenkung';
%tab1.speed_panel.FontSize = 18;

tab1.steering_control = uislider(tab1.steering_panel);
tab1.steering_control.Position = [18, 72, slider_size(1), 3];
tab1.steering_control.Limits = [-100, 100];
tab1.steering_control.ValueChangedFcn = @cb.steeringChange;

% tab1 - sensor  ------------------------------------------------------------------------------------------------------------------

sensor_panel_size = [tab_size(1) - speed_panel_size(1) - 3 * 24, 2/3 * tab_size(2) - reload_button_size(2)];
axis_size = [sensor_panel_size(1) - 16, 1/3 * (sensor_panel_size(2) - title_space) - 8];

tab1.sensor_panel = uipanel(tab1.tab);
tab1.sensor_panel.Position = [speed_panel_size(1) + 48, 1/3 * tab_size(2) - 24  + reload_button_size(2), sensor_panel_size(1),  sensor_panel_size(2)];
tab1.sensor_panel.BorderType = 'none';
tab1.sensor_panel.Title = 'Sensoren';

tab1.ir_front = uiaxes(tab1.sensor_panel);
tab1.ir_front.Position = [8, sensor_panel_size(2) - title_space - axis_size(2) - 8, axis_size(1), axis_size(2)];
tab1.ir_front.Tag = "IR Front Sensor";

tab1.ir_left = uiaxes(tab1.sensor_panel);
tab1.ir_left.Position = [8, sensor_panel_size(2) - title_space - 2 * axis_size(2) - 8, axis_size(1), axis_size(2)];
tab1.ir_left.Tag = "IR Left Sensor";

tab1.ir_right = uiaxes(tab1.sensor_panel);
tab1.ir_right.Position = [8, sensor_panel_size(2) - title_space - 3 * axis_size(2) - 8, axis_size(1), axis_size(2)];
tab1.ir_right.Tag = "IR Right Sensor";

% tab1 - battery ------------------------------------------------------------------------------------------------------------------

battery_panel_size = [tab_size(1) - speed_panel_size(1) - 3 * 24, 1/3 * tab_size(2) - 3 * 24];
battery_size = [battery_panel_size(1) - 36, battery_panel_size(2) - 56];

bg_color = 'w';
fg_color = 'g';

tab1.battery_panel = uipanel(tab1.tab);
tab1.battery_panel.Position = [speed_panel_size(1) + 48, 24 + reload_button_size(2), battery_panel_size(1), battery_panel_size(2)];
tab1.battery_panel.BorderType = 'none';
tab1.battery_panel.Title = 'Batteriestand';

tab1.battery = uiaxes(tab1.battery_panel);
tab1.battery.Position = [(battery_panel_size(1) - battery_size(1)) - 8, (battery_panel_size(2) - battery_size(2)) / 2, battery_size(1), battery_size(2)];
tab1.battery.Tag = "IR Battery Sensor";

tab1.battery.Units = 'pixels';
tab1.battery.XLim = [0 1];
tab1.battery.YLim = [0 1];
tab1.battery.XTick = [];
tab1.battery.YTick = [];
tab1.battery.Color = bg_color;
tab1.battery.XColor = bg_color;
tab1.battery.YColor = bg_color;

tab1.battery_value = patch([0 .5 .5 0],[0 0 1 1],fg_color, ...
                        'Parent', tab1.battery,...
                        'EdgeColor','none',...
                        'EraseMode','none');

% tab1 - reload

tab1.reload_btn = uibutton(tab1.tab);
tab1.reload_btn.Position = [24, 24, reload_button_size(1), reload_button_size(2)];
tab1.reload_btn.Text = 'Reload';
tab1.reload_btn.ButtonPushedFcn = @cb.reload;
                    
envHandle.tab1 = tab1;

% tab2 - gyroscope ------------------------------------------------------------------------------------------------------------------

general_panel_size =  [(tab_size(1) / 2) - 36, tab_size(2) - 48];
gyro_plot_size = [general_panel_size(1) - 32, (general_panel_size(2) / 3)];

tab2.gyro_panel = uipanel(tab2.tab);
tab2.gyro_panel.Position = [24, 24, general_panel_size(1), general_panel_size(2)];
tab2.gyro_panel.Title = 'Gyroskop';
tab2.gyro_panel.BorderType = 'none';

tab2.gyro_x_label = uilabel(tab2.gyro_panel);
tab2.gyro_x_label.Position = [8, (gyro_plot_size(2) * 5/2) - 8, 16, 16];
tab2.gyro_x_label.Text = '<b style="color:blue;">X</b>';
tab2.gyro_x_label.Interpreter = 'html';

tab2.gyro_x_plot = uiaxes(tab2.gyro_panel);
tab2.gyro_x_plot.Position = [32, (gyro_plot_size(2) * 2), gyro_plot_size(1), gyro_plot_size(2) - 24];
tab2.gyro_x_plot.Tag = "Gyro X";
tab2.gyro_x_plot.YLim = [-180 180];

tab2.gyro_y_label = uilabel(tab2.gyro_panel);
tab2.gyro_y_label.Position = [8, (gyro_plot_size(2) * 3/2) - 8, 16, 16];
tab2.gyro_y_label.Text = '<b style="color:green;">Y</b>';
tab2.gyro_y_label.Interpreter = 'html';

tab2.gyro_y_plot = uiaxes(tab2.gyro_panel);
tab2.gyro_y_plot.Position = [32, gyro_plot_size(2), gyro_plot_size(1), gyro_plot_size(2) - 24];
tab2.gyro_y_plot.Tag = "Gyro Y";
tab2.gyro_y_plot.YLim = [-180 180];

tab2.gyro_z_label = uilabel(tab2.gyro_panel);
tab2.gyro_z_label.Position = [8, (gyro_plot_size(2) / 2) - 8, 16, 16];
tab2.gyro_z_label.Text = '<b style="color:red;">Z</b>';
tab2.gyro_z_label.Interpreter = 'html';

tab2.gyro_z_plot = uiaxes(tab2.gyro_panel);
tab2.gyro_z_plot.Position = [32, 0, gyro_plot_size(1), gyro_plot_size(2) - 24];
tab2.gyro_z_plot.Tag = "Gyro Z";
tab2.gyro_z_plot.YLim = [-180 180];

% tab2 - accelerometer ------------------------------------------------------------------------------------------------------------------

tab2.pos_panel = uipanel(tab2.tab);
tab2.pos_panel.Position = [tab_size(1) / 2 + 12, 24, general_panel_size(1), general_panel_size(2)];
tab2.pos_panel.Title = 'Position';
tab2.pos_panel.BorderType = 'none';

tab2.pos_x_label = uilabel(tab2.pos_panel);
tab2.pos_x_label.Position = [8, (gyro_plot_size(2) * 5/2) - 8, 16, 16];
tab2.pos_x_label.Text = '<b style="color:blue;">X</b>';
tab2.pos_x_label.Interpreter = 'html';

tab2.pos_x_plot = uiaxes(tab2.pos_panel);
tab2.pos_x_plot.Position = [32, (gyro_plot_size(2) * 2), gyro_plot_size(1), gyro_plot_size(2) - 24];
tab2.pos_x_plot.Tag = "Pos X";

tab2.pos_y_label = uilabel(tab2.pos_panel);
tab2.pos_y_label.Position = [8, (gyro_plot_size(2) * 3/2) - 8, 16, 16];
tab2.pos_y_label.Text = '<b style="color:green;">Y</b>';
tab2.pos_y_label.Interpreter = 'html';

tab2.pos_y_plot = uiaxes(tab2.pos_panel);
tab2.pos_y_plot.Position = [32, gyro_plot_size(2), gyro_plot_size(1), gyro_plot_size(2) - 24];
tab2.pos_y_plot.Tag = "Pos Y";

envHandle.tab2 = tab2;


% #########################
% #          tab3         #
% #########################

% tab3 - speed ------------------------------------------------------------------------------------------------------------------

tab3.speed_panel = uipanel(tab3.tab);
tab3.speed_panel.Position = [24, tab_size(2) - speed_panel_size(2) - 24, speed_panel_size(1), speed_panel_size(2)];
tab3.speed_panel.BorderType = 'none';
tab3.speed_panel.Title = 'Geschwindigkeit';
%tab1.speed_panel.FontSize = 18;

tab3.rpm = uigauge(tab3.speed_panel);
tab3.rpm.Position = [(speed_panel_size(1) - gauge_size(1))/2, speed_panel_size(2) - gauge_size(2) - title_space, gauge_size(1), gauge_size(2)];
tab3.rpm.Limits = [0, 5];

tab3.speed_control = uislider(tab3.speed_panel);
tab3.speed_control.Position = [18, 80, slider_size(1), 3];
tab3.speed_control.Limits = [-100, 100];
tab3.speed_control.ValueChangedFcn = @cb.speedChange;

% tab3 - steering -----------------------------------------------------------------------------------------------------------------

steering_panel_size = [2/3 * tab_size(2) - 60, 1/3 * tab_size(2) - 3 * 24];
slider_size = [steering_panel_size(1) - 36, 60];

tab3.steering_panel = uipanel(tab3.tab);
tab3.steering_panel.Position = [24, 24 + reload_button_size(2), steering_panel_size(1), steering_panel_size(2)];
tab3.steering_panel.BorderType = 'none';
tab3.steering_panel.Title = 'Lenkung';
%tab1.speed_panel.FontSize = 18;

tab3.steering_control = uislider(tab3.steering_panel);
tab3.steering_control.Position = [18, 72, slider_size(1), 3];
tab3.steering_control.Limits = [-100, 100];
tab3.steering_control.ValueChangedFcn = @cb.steeringChange;

% tab3 -  cruise control  ------------------------------------------------------------------------------------------------------------------

indicator_panel_size = [sensor_panel_size(1) - 16, 1/4 * (sensor_panel_size(2) - title_space) - 8];
lamp_size = (1/16)*indicator_panel_size(1);
button_size = [(1/4)*indicator_panel_size(1), (1/2)*indicator_panel_size(2)];
speed_label_size = [indicator_panel_size(1)/2, 32];

tab3.cruise_control_panel = uipanel(tab3.tab);
tab3.cruise_control_panel.Position = [speed_panel_size(1) + 48, 1/3 * tab_size(2) - 24  + reload_button_size(2), sensor_panel_size(1),  sensor_panel_size(2)];
tab3.cruise_control_panel.BorderType = 'none';
tab3.cruise_control_panel.Title = 'Tempomat';

% tab3 - ref speed ------------------------------------------------------------------------------------------------------------------

tab3.cc_speed_panel = uipanel(tab3.cruise_control_panel);
tab3.cc_speed_panel.Position = [8, sensor_panel_size(2) - title_space - indicator_panel_size(2) - 8, indicator_panel_size(1), indicator_panel_size(2)];
tab3.cc_speed_panel.BorderType = 'none';

tab3.ref_speed_label = uilabel(tab3.cc_speed_panel);
tab3.ref_speed_label.Position = [8, indicator_panel_size(2)/2 - speed_label_size(2)/2, speed_label_size(1), speed_label_size(2)];
tab3.ref_speed_label.Text = '<b> Reference Speed (m/s): </b>';
tab3.ref_speed_label.FontSize = 20;
tab3.ref_speed_label.Interpreter = 'html';

tab3.ref_speed = uilabel(tab3.cc_speed_panel);
tab3.ref_speed.Position = [16 + indicator_panel_size(1)/2, indicator_panel_size(2)/2 - speed_label_size(2)/2, speed_label_size(1), speed_label_size(2)];
tab3.ref_speed.Text = '0';
tab3.ref_speed.FontSize = 20;


% tab3 - cc activate ------------------------------------------------------------------------------------------------------------------

tab3.cc_activate_panel = uipanel(tab3.cruise_control_panel);
tab3.cc_activate_panel.Position = [8, sensor_panel_size(2) - title_space - 2 * indicator_panel_size(2) - 8, indicator_panel_size(1), indicator_panel_size(2)];
tab3.cc_activate_panel.BorderType = 'none';

% tab3 - cc activate - cruise  ------------------------------------------------------------------------------------------------------------------

tab3.indicator_lamp_cruise = uilamp(tab3.cc_activate_panel);
tab3.indicator_lamp_cruise.Position = [8, indicator_panel_size(2)/2 - lamp_size/2, lamp_size,lamp_size];
tab3.indicator_lamp_cruise.Enable = 'off';

tab3.indicator_button_cruise = uibutton(tab3.cc_activate_panel, 'push');
tab3.indicator_button_cruise.Position = [16 + lamp_size, indicator_panel_size(2)/2 - lamp_size/2, button_size(1), button_size(2)];
tab3.indicator_button_cruise.BackgroundColor = [0.902 0.902 0.902];
tab3.indicator_button_cruise.FontSize = 16;
tab3.indicator_button_cruise.Text = "cruise";
tab3.indicator_button_cruise.FontWeight = 'bold';
tab3.indicator_button_cruise.ButtonPushedFcn = @cb.cruise;

% tab3 - cc activate - cancle  ------------------------------------------------------------------------------------------------------------------

%tab3.indicator_lamp_cancle = uilamp(tab3.indicator_cruise);
%tab3.indicator_lamp_cancle.Position = [8 + indicator_panel_size(1)/2, indicator_panel_size(2)/2 - lamp_size/2, lamp_size,lamp_size];
%tab3.indicator_lamp_cancle.Color = [1 0 0];
%tab3.indicator_lamp_cancle.Enable = 'off';

tab3.indicator_button_cancle = uibutton(tab3.cc_activate_panel, 'push');
tab3.indicator_button_cancle.Position = [16 + indicator_panel_size(1)/2, indicator_panel_size(2)/2 - lamp_size/2, button_size(1), button_size(2)];
tab3.indicator_button_cancle.BackgroundColor = [0.902 0.902 0.902];
tab3.indicator_button_cancle.FontSize = 16;
tab3.indicator_button_cancle.Text = "cancle";
tab3.indicator_button_cancle.FontWeight = 'bold';
tab3.indicator_button_cancle.ButtonPushedFcn = @cb.cancle;

% tab3 - cc enable ------------------------------------------------------------------------------------------------------------------

tab3.cc_enable_panel = uipanel(tab3.cruise_control_panel);
tab3.cc_enable_panel.Position = [8, sensor_panel_size(2) - title_space - 3 * indicator_panel_size(2) - 8, indicator_panel_size(1), indicator_panel_size(2)];
tab3.cc_enable_panel.BorderType = 'none';

% tab3 - cc enable - set  ------------------------------------------------------------------------------------------------------------------

tab3.indicator_lamp_set = uilamp(tab3.cc_enable_panel);
tab3.indicator_lamp_set.Position = [8, indicator_panel_size(2)/2 - lamp_size/2, lamp_size, lamp_size];
tab3.indicator_lamp_set.Color = [1 1 0.0667];
tab3.indicator_lamp_set.Enable = 'off';

tab3.indicator_button_set = uibutton(tab3.cc_enable_panel, 'push');
tab3.indicator_button_set.Position = [16 + lamp_size, indicator_panel_size(2)/2 - lamp_size/2, button_size(1), button_size(2)];
tab3.indicator_button_set.BackgroundColor = [0.902 0.902 0.902];
tab3.indicator_button_set.FontSize = 16;
tab3.indicator_button_set.Text = "set";
tab3.indicator_button_set.FontWeight = 'bold';
tab3.indicator_button_set.ButtonPushedFcn = @cb.set;

% tab3 - cc enable - resume  ------------------------------------------------------------------------------------------------------------------

%tab3.indicator_lamp_resume = uilamp(tab3.indicator_set);
%tab3.indicator_lamp_resume.Position = [8 + indicator_panel_size(1)/2, indicator_panel_size(2)/2 - lamp_size/2, lamp_size,lamp_size];
%tab3.indicator_lamp_resume.Color = [1 0.0745 0.651];
%tab3.indicator_lamp_resume.Enable = 'off';

tab3.indicator_button_resume = uibutton(tab3.cc_enable_panel, 'push');
tab3.indicator_button_resume.Position = [16 + indicator_panel_size(1)/2, indicator_panel_size(2)/2 - lamp_size/2, button_size(1), button_size(2)];
tab3.indicator_button_resume.BackgroundColor = [0.902 0.902 0.902];
tab3.indicator_button_resume.FontSize = 16;
tab3.indicator_button_resume.Text = "resume";
tab3.indicator_button_resume.FontWeight = 'bold';
tab3.indicator_button_resume.ButtonPushedFcn = @cb.resume;

% tab3 - cc inc dec ------------------------------------------------------------------------------------------------------------------

tab3.cc_inc_dec_panel = uipanel(tab3.cruise_control_panel);
tab3.cc_inc_dec_panel.Position = [8, sensor_panel_size(2) - title_space - 4 * indicator_panel_size(2) - 8, indicator_panel_size(1), indicator_panel_size(2)];
tab3.cc_inc_dec_panel.BorderType = 'none';

% tab3 - cc inc dec - inc  ------------------------------------------------------------------------------------------------------------------

tab3.indicator_button_inc = uibutton(tab3.cc_inc_dec_panel, 'state');
tab3.indicator_button_inc.Position = [16, indicator_panel_size(2)/2 - lamp_size/2, button_size(1) + lamp_size, button_size(2)];
tab3.indicator_button_inc.BackgroundColor = [0.3922 0.8314 0.0745];
tab3.indicator_button_inc.FontSize = 16;
tab3.indicator_button_inc.Text = "increase";
tab3.indicator_button_inc.FontWeight = 'bold';
tab3.indicator_button_inc.ValueChangedFcn = @cb.increase;

% tab3 - cc inc dec - dec  ------------------------------------------------------------------------------------------------------------------

tab3.indicator_button_dec = uibutton(tab3.cc_inc_dec_panel, 'state');
tab3.indicator_button_dec.Position = [16+indicator_panel_size(1)/2, indicator_panel_size(2)/2 - lamp_size/2, button_size(1) + lamp_size, button_size(2)];
tab3.indicator_button_dec.BackgroundColor = [1 0.4118 0.1608];
tab3.indicator_button_dec.FontSize = 16;
tab3.indicator_button_dec.Text = "decrease";
tab3.indicator_button_dec.FontWeight = 'bold';
tab3.indicator_button_dec.ValueChangedFcn = @cb.decrease;

envHandle.tab3 = tab3;

end







function data = setSensorGraph (data, time)
    global envHandle
    
    envHandle.tab1.ir_front.Children.XData = time;
    envHandle.tab1.ir_left.Children.XData = time;
    envHandle.tab1.ir_right.Children.XData = time;
    
    envHandle.tab1.ir_front.Children.YData = data.frontSensor;
    envHandle.tab1.ir_left.Children.YData = data.leftSensor;
    envHandle.tab1.ir_right.Children.YData = data.rightSensor;
    
    return
end
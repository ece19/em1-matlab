function data = setGyroGraph (data, time)
    global envHandle
    
    envHandle.tab2.gyro_x_plot.Children.XData = time;
    envHandle.tab2.gyro_y_plot.Children.XData = time;
    envHandle.tab2.gyro_z_plot.Children.XData = time;
    
    envHandle.tab2.gyro_x_plot.Children.YData   = data.gyro_x;
    envHandle.tab2.gyro_y_plot.Children.YData   = data.gyro_y;
    envHandle.tab2.gyro_z_plot.Children.YData   = data.gyro_z;
    
    return
end
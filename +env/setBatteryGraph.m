function setBatteryGraph (val)
    global envHandle;

    env.setProgressbar(envHandle.tab1.battery_value, val);
end
function toggleLight(val, light_name)
    global envHandle;

    if light_name == 1
        tmp_handle = envHandle.tab3.indicator_lamp_cruise;
    elseif light_name == 2
        tmp_handle = envHandle.tab3.indicator_lamp_cancle;
    elseif light_name == 3
        tmp_handle = envHandle.tab3.indicator_lamp_set;
    elseif light_name == 4
        tmp_handle = envHandle.tab3.indicator_lamp_resume;
    end

    if val
        tmp_handle.Enable = 'on';
    else
        tmp_handle.Enable = 'off';
    end
   
end
